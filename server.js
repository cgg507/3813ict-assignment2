"use strict";

let ROOT_PATH = __dirname + '/dist/frontend';
const PORT = process.env.PORT || 3000;
const mongoUrl = 'mongodb://localhost:27017';
const dbName = 'chatter';

const express = require('express');
const MongoClient = require('mongodb').MongoClient;
const http = require('http');
const bodyParser = require('body-parser');
const fs = require('fs');


const apiRouter = require("./server/routes/api_router");
const userRouter = require("./server/routes/user_router");
const groupRouter = require("./server/routes/group_router");
const channelRouter = require("./server/routes/channel_router");
const messageRouter = require("./server/routes/message_router");
const socketRouter = require("./server/routes/socket_router");
const staticRouter = require("./server/routes/static_router");
const databaseBuild = require("./server/includes/database").databaseBuild;
const Users = require("./server/models/users");
const Messages = require("./server/models/messages");

let socketIO = require('socket.io');
const ApiResponse = require("./server/models/api_response");


var db = null;
let app = express();
let server = http.Server(app);
let io = socketIO(server);

// Use connect method to connect to the server
MongoClient.connect(mongoUrl, function (err, client) {
  console.log("Connected successfully to mongodb: " + dbName);

  db = client.db(dbName);
  //Attach the DB object to requests
  databaseBuild(db);
  setupRoutes();
  startHTTPServer();
  try {
    fs.mkdirSync(__dirname + "/uploads")
  } catch (err) {
    if (err.code !== 'EEXIST') {
      console.error(err);
    }
  }
});


function setupRoutes() {
//Attach the DB object to requests
  app.use(function (req, res, next) {
    req.userService = require("./server/services/user_service")(db);
    req.sessionService = require("./server/services/session_service")(db);
    req.groupService = require("./server/services/group_service")(db);
    req.channelService = require("./server/services/channel_service")(db);
    req.messageService = require("./server/services/message_service")(db);

    //This fires after the request is done. Allows us to save the data
    res.on("finish", function () {

    });
    next();
  });

  console.log("Going to serve: " + __dirname + "/uploads");

  app.use(express.static(ROOT_PATH));
  app.use('/uploads', express.static(__dirname + "/uploads"));
  app.use(bodyParser.json());
  app.use('/', staticRouter(ROOT_PATH));
  app.use("/api/api", apiRouter());
  app.use("/api/user", userRouter());
  app.use("/api/group", groupRouter());
  app.use("/api/channel", channelRouter());
  app.use("/api/message", messageRouter());
}

function startHTTPServer() {
  server.listen(PORT, () => {
    var host = server.address().address;
    var port = server.address().port;
    console.log("My First Nodejs Server!");
    console.log("Server listening on: " + host + " port: " + port);
  });


  io.on('connect', (socket) => {
    socket.userService = require("./server/services/user_service")(db);
    socket.sessionService = require("./server/services/session_service")(db);
    socket.groupService = require("./server/services/group_service")(db);
    socket.channelService = require("./server/services/channel_service")(db);
    socket.messageService = require("./server/services/message_service")(db);

    socket.isAuth = false;

    socket.on('test', socketRouter(io, socket).testRoute);

    socket.on('auth', socketRouter(io, socket).authRoute);

    socket.on('join', socketRouter(io, socket).joinRoomRoute);

    socket.on('leave', socketRouter(io, socket).leaveRoomRoute);

    socket.on('message', socketRouter(io, socket).messageRoomRoute);

    socket.on('disconnecting', socketRouter(io, socket).disconnectingRoute);
  });
}


