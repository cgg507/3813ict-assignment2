import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import {Observable} from 'rxjs';
import {ApiResponse, SocketResponse} from '../models/forms/apiresponse';

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  private url = 'http://localhost:3000';
  private socket;

  constructor() {
    this.socket = io(this.url);
    this.socket.emit('test', 'Starting socket');
  }

  authenticate(token) {
    this.socket.emit('auth', token);
  }

  joinRoom(channel) {
    this.socket.emit('join', channel);
  }

  leaveRoom(channel) {
    this.socket.emit('leave', channel);
  }

  onMessage(): Observable<SocketResponse> {
    return new Observable<SocketResponse>(observer => {
      this.socket.on('message', (data: SocketResponse) => observer.next(data));
    });
  }

  onAuth(): Observable<SocketResponse> {
    return new Observable<SocketResponse>(observer => {
      this.socket.on('auth', (data: SocketResponse) => observer.next(data));
    });
  }

  onEvent(): Observable<SocketResponse> {
    return new Observable<SocketResponse>(observer => {
      this.socket.on('event', (data: SocketResponse) => observer.next(data));
    });
  }

  sendToRoom(message) {
    this.socket.emit('message', {room: message.channelId, msg: message});
  }
}
