import {ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserForm} from '../../models/forms/userform';
import {Users} from '../../models/users';
import {Groups} from '../../models/groups';
import {ApiService} from '../../service/api.service';

@Component({
  selector: 'app-userform',
  templateUrl: './userform.component.html',
  styleUrls: ['./userform.component.css']
})
export class UserformComponent implements OnInit {
  public permissions = ['admin', 'group'];
  userForm: FormGroup;
  @Output() submitForm = new EventEmitter();
  private _user = new Users();
  private _thisUser = new Users();
  @Input()
  set user(u: Users) {
    if (typeof(this.userForm) !== 'undefined') {
      this._user = u;
      this.userForm.patchValue(u);
    }
  }

  constructor(private formBuilder: FormBuilder, private cd: ChangeDetectorRef) {
    this._thisUser = JSON.parse(window.localStorage.getItem(ApiService.USERDATA)) as Users;
    this.userForm = this.formBuilder.group({
      email: [this._user.email, [Validators.required, Validators.email]],
      firstName: [this._user.firstName, [Validators.required]],
      lastName: [this._user.lastName, [Validators.required]],
      fileUpload: ['', []],
      fileName: ['', []],
      permissions: [this._user.permissions, []]
    });
  }

  ngOnInit() { }

  onFileChange(event) {
    console.log('File has changed');
    const reader = new FileReader();

    if (event.target.files && event.target.files.length) {
      console.log('Actually got a file');
      const file = event.target.files[0];
      reader.readAsDataURL(file);

      reader.onload = () => {
        this.userForm.patchValue({
          fileUpload: reader.result,
          fileName: file.name
        });

        this.cd.markForCheck();
      };
    }
  }

  successForm() {
    const form = new UserForm();

    form.email = this.userForm.get('email').value;
    form.firstName = this.userForm.get('firstName').value;
    form.lastName = this.userForm.get('lastName').value;
    form.fileUpload = this.userForm.get('fileUpload').value;
    form.fileName = this.userForm.get('fileName').value;
    if (this._thisUser.permissions.length > 0) {
      form.permissions = this.userForm.get('permissions').value;
    }
    this.submitForm.emit(form);
  }

  getUserId() {
    return this._user.id;
  }

  getUserImage() {
    return this._user.image;
  }

  isElevated() {
    return this._thisUser.permissions.length > 0;
  }

}
