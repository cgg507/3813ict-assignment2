export class GroupForm {
  name: string;
  admins: number[];
  users: number[];
  channels: number[];
}
