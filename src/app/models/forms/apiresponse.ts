import {Groups} from '../groups';
import {Users} from '../users';
import {Channels} from '../channels';
import {Messages} from '../messages';

export class ApiResponse {
  success: boolean;
  data: object;
  error: object;
}

export class LoginResponse extends ApiResponse {
  data: {session: string, user: Users};
}

export class GroupsResponse extends ApiResponse{
  data: Groups[];
}

export class GroupResponse extends ApiResponse{
  data: {
    group: Groups,
    users: Users[],
    channels: Channels[]
  };
}

export class UsersResponse extends ApiResponse {
  data: Users[];
}

export class UserResponse extends ApiResponse {
  data: Users;
}

export class ChannelsResponse extends ApiResponse {
  data: Channels[];
}

export class ChannelResponse extends ApiResponse {
  data: {
    channel: Channels,
    users: Users[],
    group: Groups
  };
}

export class MessagesResponse extends ApiResponse {
  data: Messages[];
}

export class MessageResponse extends ApiResponse {
  data: {
    message: Messages,
  };
}

export class SocketResponse extends ApiResponse {
  data: {
    event: String,
    data: Object
  };
}
