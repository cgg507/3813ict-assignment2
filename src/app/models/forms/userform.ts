export class UserForm {
  email: string;
  firstName: string;
  lastName: string;
  fileUpload: string;
  fileName: string;
  permissions: string[];
}