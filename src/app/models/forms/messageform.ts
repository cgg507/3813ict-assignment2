export class MessageForm {
  message: string;
  image: string;
  channelId: string;
  fileUpload: string = null;
  fileName: string = null;
}