import {Groups} from './groups';
import {Messages} from './messages';

export class Channels {
  id: number = null;
  name: string = null;
  users: number[] = [];
  groupId: number = null;
  group: Groups = null;
  messages: Messages[] = [];
}

