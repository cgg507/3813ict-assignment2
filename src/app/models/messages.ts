import {Users} from './users';

export class Messages {
  id: number = null;
  message: string = null;
  userId: number = null;
  fromUser: Users = null;
  groupId: number = null;
  channelId: number = null;
  image: string = null;
  createDate: Date = null;
}