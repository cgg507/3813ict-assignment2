import { Component, OnInit } from '@angular/core';
import {Groups} from '../../models/groups';
import {Router} from '@angular/router';
import {ApiService} from '../../service/api.service';

/**
 * Dashboard view to show all current channels
 * @author Charles Galvin<charles@c2dev.com.au>
 */
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  /** @var groups Groups */
  groups = [];
  channels = [];
  messages = [];

  constructor(private router: Router, private apiService: ApiService) {
    this.getGroups();
    this.getChannels();
    this.getMessages();
  }

  /**
   * Get all the groups for this user
   */
  getGroups() {
    this.apiService.getGroups().then((resp) => {
      if (resp.success) {
        this.groups = resp.data;
      } else {
        console.error('Failure to get group data');
      }
    });
  }

  /**
   * Get all messages for this user
   */
  getMessages() {
    this.apiService.getMessages().then((resp) => {
      if (resp.success) {
        this.messages = resp.data.splice(-10);
      } else {
        console.error('Failure to get group data');
      }
    });
  }

  /**
   * Get all the channels the user is subscribed to
   * @param group
   */
  getChannels() {
    this.apiService.getChannels().then((resp) => {
      if (resp.success) {
        this.channels = resp.data;
      } else {
        console.error('Failure to get channel data');
      }
    });
  }

  ngOnInit() {
  }

}
