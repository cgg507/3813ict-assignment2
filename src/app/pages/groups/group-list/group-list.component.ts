import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../../service/api.service';

/**
 * A page to show all the groups
 * @author Charles Galvin<charles@c2dev.com.au>
 */
@Component({
  selector: 'app-group-list',
  templateUrl: './group-list.component.html',
  styleUrls: ['./group-list.component.css']
})
export class GroupListComponent implements OnInit {
  /** @var groups Groups[] */
  groups = [];

  constructor(public apiService: ApiService) {
    this.loadGroups();
  }

  /**
   * Hit the api endpoint to get current groups for the user
   */
  loadGroups() {
    this.apiService.getGroups().then((response) => {
      this.groups = response.data;
    });
  }

  ngOnInit() {
  }

  /**
   * Delete a group based on ID
   * @param id
   */
  deleteGroup(id: Number) {
    this.apiService.deleteGroup(id).then((resp) => {
      if (resp.success) {
        alert('Successfully removed group');
        this.loadGroups();
      } else {
        alert('Failed to delete group.');
      }
    });
  }

  /**
   * See if the current user has group editing permissions
   * @param g Groups
   */
  checkGroupPerms(g) {
    if (this.apiService.canUser('group') && g.admins.includes(this.apiService.getStoredUser().id)) {
      return true;
    }
    if (this.apiService.canUser('admin')) {
      return true;
    }
    return false;
  }

}
