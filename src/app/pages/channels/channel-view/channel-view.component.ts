import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {Messages} from '../../../models/messages';
import {Channels} from '../../../models/channels';
import {ActivatedRoute, Router} from '@angular/router';
import {ApiService} from '../../../service/api.service';
import {SocketService} from '../../../service/socket.service';
import {Users} from '../../../models/users';
import {ApiResponse, SocketResponse} from '../../../models/forms/apiresponse';
import {FormBuilder} from '@angular/forms';
import {MessageForm} from '../../../models/forms/messageform';

/**
 * A page to view channel messages
 * @author Charles Galvin<charles@c2dev.com.au>
 */
@Component({
  selector: 'app-channel-view',
  templateUrl: './channel-view.component.html',
  styleUrls: ['./channel-view.component.css']
})
export class ChannelViewComponent implements OnInit, OnDestroy {
  channel: Channels = null;
  messages: Messages[] = [];
  newMessage = '';
  fileName = null;
  fileData = null;
  users: Users[] = [];

  constructor(private router: Router,
              public apiService: ApiService,
              private route: ActivatedRoute,
              public socketService: SocketService,
              private cd: ChangeDetectorRef) {
    const channelId = this.route.snapshot.paramMap.get('id');
    if (channelId) {
      this.socketService.onAuth().subscribe((message) => {
        console.log('Got auth response');
        socketService.joinRoom('channel-' + this.channel.id);
      });

      this.apiService.getChannel(channelId).then((resp) => {
        if (resp.success) {
          this.channel = resp.data.channel;
          this.getAllMessages();
          const userToken = localStorage.getItem(ApiService.USERTOKEN);
          socketService.authenticate(userToken);
        } else {
          console.error('Failed to get channel data');
        }
      });

      this.socketService.onMessage().subscribe((message) => {
        console.log('Getting message event');
        console.log(message);
        this.messages.push(message.data.data as Messages);
      });

      this.socketService.onEvent().subscribe((message) => {
        const event = message as SocketResponse;
        if (event.success) {
          if (event.data.event === 'userJoin') {
            this.users.push(event.data.data as Users);
          }
          if (event.data.event === 'userLeave') {
            this.users = this.users.filter((data) => {
              if (data.id !== event.data.data as Number) {
                return true;
              }
              return false;
            });
          }
        } else {
          console.error('There was an event, but there was a server failure');
        }
      });
    }
  }

  ngOnInit() {

  }

  onFileChange(event) {
    console.log('File has changed');
    const reader = new FileReader();

    if (event.target.files && event.target.files.length) {
      console.log('Actually got a file');
      const file = event.target.files[0];
      reader.readAsDataURL(file);

      reader.onload = () => {
        const newMess = new MessageForm();

        newMess.channelId = 'channel-' + this.channel.id;
        newMess.fileName = file.name;
        newMess.fileUpload = reader.result;
        newMess.message = this.newMessage;
        const fileName = file.name;
        this.socketService.sendToRoom(newMess);
        this.newMessage = '';
        this.cd.markForCheck();
      };
    }
  }

  ngOnDestroy() {
    console.log('Leaving the page');
    this.socketService.leaveRoom('channel-' + this.channel.id);
  }

  sendMessage() {
    if (this.channel.id != null) {
      console.log('Sending message');
      const newMess = new MessageForm();
      newMess.channelId = 'channel-' + this.channel.id;
      newMess.message = this.newMessage;
      this.socketService.sendToRoom(newMess);
      this.newMessage = '';
    }
  }

  getAllMessages() {
    if (this.channel != null) {
      this.apiService.getMessages('?channelId=' + this.channel.id).then((resp) => {
        if (resp.success !== false) {
          console.log(resp);
          this.messages = resp.data;
        }
      });
    }
  }

}
