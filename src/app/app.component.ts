import {Component} from '@angular/core';
import {SocketService} from './service/socket.service';
import {ApiService} from './service/api.service';

declare var $: any; // Super special jquery stuff

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'frontend';

  constructor(socketService: SocketService, apiService: ApiService) {
    const oldUser = apiService.getStoredUser();
    if (oldUser != null) {
      apiService.getUser(oldUser.id).then((resp) => {
        window.localStorage.setItem(ApiService.USERDATA, JSON.stringify(resp.data));
      });
    }
  }

}
