'use strict';

const ApiResponse = require("../models/api_response");
const GROUP_PERM = 'group';
const ADMIN_PERM = 'admin';

const atob = require('atob');

module.exports.isAuthenticated = function(req, res, next) {
  //Check for the header
  if (typeof(req.headers.authorization) !== undefined && req.headers.authorization != null) {
    const auth = req.headers.authorization.split(" ");
    if (auth.length === 2 && auth[1] !== "") {
      let session = atob(auth[1]);
      req.sessionService.checkSession(session).then((data) => {
        if (data == null) {
          let resp = new ApiResponse();
          resp.error = ["Authentication not accepted"];
          res.send(JSON.stringify(resp));
          //next("Authentication not accepted");
        } else {
          req.sessionToken = session;
          req.userService.getUserByToken(req.sessionToken).then((data) => {
            req.thisUser = data;
            next();
          }, (err) => {
            let resp = new ApiResponse();
            resp.error = ["Authentication failure"];
            res.send(JSON.stringify(resp));
          });
        }
      }, (err) => {
        let resp = new ApiResponse();
        resp.error = ["Authentication failure"];
        res.send(JSON.stringify(resp));
      });
    }
  }

};

module.exports.hasGroupWrite = function(req, res, next) {
  /** @var thisUser Users */
  req.userService.getUserByToken(req.sessionToken).then((thisUser) => {
    if (thisUser.permissions.includes(ADMIN_PERM)) {
      return next();
    }
    if (thisUser.permissions.includes(GROUP_PERM)) {
      let id = parseInt(req.params['id']);
      if (id > 0) {
        req.groupService.getGroup(id).then((g) => {
          if (g.admins.includes(thisUser.id)) {
            return next();
          }
        }, (err) => {
          let resp = new ApiResponse();
          resp.error = ["Permission denied"];
          res.send(resp);
        });
      } else {
        return next();
      }
    } else {
      let resp = new ApiResponse();
      resp.error = ["Permission denied"];
      res.send(resp);
    }
  }, (err) => {
    let resp = new ApiResponse();
    resp.error = ["Permission denied"];
    res.send(resp);
  });
};

module.exports.hasChannelWrite = function(req, res, next) {
  /** @var thisUser Users */
  req.userService.getUserByToken(req.sessionToken).then((thisUser) => {
    if (thisUser !== null) {
      if (thisUser.permissions.includes(ADMIN_PERM)) {
        return next();
      } else if (thisUser.permissions.includes(GROUP_PERM)) {
        let id = parseInt(req.params['id']);
        if (id > 0) {
          req.channelService.getChannel(id).then((c) => {
            req.groupService.getGroup(c.groupId).then((g) => {
              if (g.admins.includes(thisUser.id)) {
                return next();
              }
            }, (err) => {
              let resp = new ApiResponse();
              resp.error = ["Permission denied"];
              res.send(resp);
            });
          }, (err) => {
            resp.error = ["Database failure"];
            res.send(resp);
          });
        } else {
          return next();
        }
      } else {
        let resp = new ApiResponse();
        resp.error = ["Permission denied"];
        res.send(resp);
      }
    }
  }, (err) => {
    let resp = new ApiResponse();
    resp.error = ["Permission denied"];
    res.send(resp);
  });
};

module.exports.hasUserWrite = function(req, res, next) {
  /** @var thisUser Users */
  req.userService.getUserByToken(req.sessionToken).then((thisUser) => {
    if (thisUser !== null) {
      if (thisUser.permissions.includes(ADMIN_PERM)) {
        return next();
      }
      // Let users edit themselves
      let id = parseInt(req.params['id']);
      if (thisUser.id == id) {
        return next();
      }
    }

    let resp = new ApiResponse();
    resp.error = ["Permission denied"];
    res.send(resp);
  }, (err) => {
    let resp = new ApiResponse();
    resp.error = ["Permission denied"];
    res.send(resp);
  });
};