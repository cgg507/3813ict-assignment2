
module.exports.databaseBuild = (db) => {
  db.listCollections().toArray(function(err, names) {
    console.log(names);
    console.log(err);
    if (names.findIndex((ele) => { return ele.name === 'users'}) === -1) {
      db.createCollection('users', function (err, result) {
        if (err) {
          console.error(err);
        } else {
          const userService = require("./server/services/user_service")(db);
          const superUser = new Users(1, 'admin@assignment', 'password', 'admin', 'assignment', ['admin']);
          userService.insertUser(superUser).then((data) => {
            console.log("Inserted primary super admin");
          }, (err) => {
            console.error("Database error creating superuser");
            console.log(err);
          });
        }
      });
    }
    if (names.findIndex((ele) => { return ele.name === 'groups'}) === -1) {
      db.createCollection('groups', function (err, result) {
        console.error(err)
      });
    }
    if (names.findIndex((ele) => { return ele.name === 'sessions'}) === -1) {
      db.createCollection('sessions', function (err, result) {
        console.error(err)
      });
    }
    if (names.findIndex((ele) => { return ele.name === 'channels'}) === -1) {
      db.createCollection('channels', function (err, result) {
        console.error(err)
      });
    }
    if (names.findIndex((ele) => { return ele.name === 'messages'}) === -1) {
      db.createCollection('messages', function (err, result) {
        console.error(err)
      });
    }
  });
};