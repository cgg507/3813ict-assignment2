"use strict";

const GROUP_PERM = 'group';
const ADMIN_PERM = 'admin';

const express = require('express');
const router = express.Router();
const ApiResponse = require("../models/api_response");
const isAuthenticated = require("../includes/auth").isAuthenticated;
const Messages = require("../models/messages");


function checkUserAuth(data, thisUser) {
  if (data.userId === thisUser.id) {
    return true;
  }
  if (thisUser.permissions.includes(ADMIN_PERM)) {
    return true;
  }
  if (thisUser.permissions.includes(GROUP_PERM)) {
    return true;
  }
  return false;
}

module.exports = function () {
  router.get('/', isAuthenticated, function (req, res) {
    console.log("Request: /message get");
    res.setHeader('Content-Type', 'application/json');
    let response = new ApiResponse();
    let query = null;
    if (req.query.channelId) {
      query = req.query.channelId;
    }
    req.messageService.getMessages(query).then(async (messageData) => {
      response.data = messageData.filter((data) =>{
        return query != null || checkUserAuth(data, req.thisUser);
      });
      for (let i in response.data) {
        try {
          response.data[i].fromUser = await req.userService.getUser(response.data[i].userId, true);
        } catch(err) {
          console.error(err);
        }
      }
      response.success = true;

      res.send(response);
    }, (err) => {
      res.send(response);
    });

  });

  router.get('/:id', isAuthenticated, function (req, res) {
    console.log("Request: /message get");
    res.setHeader('Content-Type', 'application/json');
    const id = parseInt(req.params['id']);

    let response = new ApiResponse();
    req.messageService.getMessage(id).then(async (data) => {
      response.data = {'message': data};
      if (!checkUserAuth(data, req.thisUser)) {
        response.data = {message: null};
      } else {
        response.data.message.fromUser = await req.userService.getUser(response.data[i].userId);
        response.success = true;
      }
      res.send(response);
    }, (err) => {
      res.send(response);
    });
  });


  router.delete('/:id', isAuthenticated, function (req, res) {
    let response = new ApiResponse();
    console.log("Request: /message delete");
    res.setHeader('Content-Type', 'application/json');
    const id = parseInt(req.params['id']);
    req.messageService.getMessage(id).then((data) => {
      if (checkUserAuth(data, req.thisUser)) {
        req.messageService.deleteMessage(id).then((data) => {
          // TODO: Check the data deleted correctly
          res.send(response);
        });
      } else {
        res.send(response);
      }
    }, (err) => {
      res.send(response);
    });
  });

  return router;
};