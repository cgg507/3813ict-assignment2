"use strict";

const GROUP_PERM = 'group';
const ADMIN_PERM = 'admin';

const express = require('express');
const router = express.Router();
const ApiResponse = require("../models/api_response");
const isAuthenticated = require("../includes/auth").isAuthenticated;
const hasChannelWrite = require("../includes/auth").hasChannelWrite;
const Channels = require("../models/channels");


function checkUserAuth(data, thisUser) {
  if (data.users.includes(thisUser.id)) {
    return true;
  }
  if (thisUser.permissions.includes(ADMIN_PERM)) {
    return true;
  }
  if (thisUser.permissions.includes(GROUP_PERM)) {
    return true;
  }
  return false;
}

module.exports = function () {
  router.get('/', isAuthenticated, function (req, res) {
    console.log("Request: /channel get");
    res.setHeader('Content-Type', 'application/json');
    let query = null;
    console.log(req.query);
    if (req.query.groupId) {
      query = req.query.groupId;
    }
    req.channelService.getChannels(query).then(async (channelData) => {
      let response = new ApiResponse();

      response.data = channelData.filter((data) =>{
        return checkUserAuth(data, req.thisUser);
      });
      for (let i in response.data) {
        try {
          response.data[i].group = await req.groupService.getGroup(response.data[i].groupId);
        } catch(err) {
          console.error(err);
        }
      }
      response.success = true;

      res.send(response);
    }, (err) => {
      resp.error = ["Permission denied"];
      res.send(resp);
    });

  });

  router.get('/:id', isAuthenticated, function (req, res) {
    console.log("Request: /channel/:id get");
    res.setHeader('Content-Type', 'application/json');
    const id = parseInt(req.params['id']);

    req.channelService.getChannel(id).then(async (data) => {
      let response = new ApiResponse();
      if (data !== null) {
        response.data = {channel: data};
        response.data.channel.group = await req.groupService.getGroup(response.data.channel.groupId);
        response.data.channel.messages = await req.messageService.getMessages(response.data.channel.id);
        if (!checkUserAuth(data, req.thisUser)) {
          response.data = {channel: null};
        } else {
          response.success = true;
        }
        res.send(response);
      } else {
        res.send(response);
      }
    }, (err) => {
      resp.error = ["Permission denied"];
      res.send(resp);
    });



  });

  router.post('/', isAuthenticated, hasChannelWrite, function (req, res) {
    let response = new ApiResponse();
    console.log("Request: /channel post");
    res.setHeader('Content-Type', 'application/json');

    let channel = Channels.loadFromPost(req.body);
    req.channelService.insertChannel(channel).then((data) => {
      // TODO: Check the data saved nicely
      response.success = true;

      res.send(response);
    }, (err) => {
      resp.error = ["Database failure"];
      res.send(resp);
    });


  });

  router.delete('/:id', isAuthenticated, hasChannelWrite, function (req, res) {
    let response = new ApiResponse();
    console.log("Request: /channel delete");
    res.setHeader('Content-Type', 'application/json');
    const id = parseInt(req.params['id']);
    req.channelService.deleteChannel(id).then((data) => {
      response.success = true;
      // TODO: Check the data deleted correctly
      res.send(response);
    }, (err) => {
      resp.error = ["Database failure"];
      res.send(resp);
    });
  });

  router.put('/:id', isAuthenticated, hasChannelWrite, function (req, res) {
    let response = new ApiResponse();
    console.log("Request: /channel put");
    res.setHeader('Content-Type', 'application/json');
    const id = parseInt(req.params['id']);

    let channel = Channels.loadFromPost(req.body);
    channel.id = id;
    req.channelService.updateChannel(channel).then((data) => {
      // TODO: Check the data saved nicely
      response.success = true;
      res.send(response);
    }, (err) => {
      resp.error = ["Database failure"];
      res.send(resp);
    });


  });

  return router;
};