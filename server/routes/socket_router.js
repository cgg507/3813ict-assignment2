
const ApiResponse = require("../models/api_response");
const Messages = require("../models/messages");
const FileUploader = require("../models/fileuploader");

module.exports = function(io, socket) {
  return {
    testRoute: (message) => {
      console.log("Getting test message");
      console.log(message);
    },

    authRoute: (message) => {
      const resp = new ApiResponse();
      if (socket.isAuth) {
        resp.success = true;
        socket.emit('auth', resp);
      } else {
        console.log("User is authenticating through socket");
        socket.userService.getUserByToken(message).then((user) => {
          socket.isAuth = true;
          socket.userId = user.id;
          resp.success = true;
          socket.emit('auth', resp);
        }, (err) => {
          console.error("Database failure in socket auth");
          socket.emit('auth', resp);
        });
      }
    },

    joinRoomRoute: (channel) => {
      console.log("User joining channel " + channel);
      if (socket.isAuth) {
        socket.join(channel, async () => {
          const resp = new ApiResponse();
          try {
            const userData = await socket.userService.getUser(socket.userId);

            resp.success = true;
            resp.data = {event: 'userJoin', data: userData};
          } catch (err) {
            console.error(err);
          }

          io.sockets.in(channel).emit("event", resp);
        });
      } else {
        console.error('User is trying to access room without auth');
      }
    },

    leaveRoomRoute: (channel) => {
      console.log("User has requested leave");
      socket.leave(channel, () => {
        const resp = new ApiResponse();
        resp.success = true;
        resp.data = {event: 'userLeave', data: socket.userId};
        io.sockets.in(channel).emit("event", resp);
      });
    },

    messageRoomRoute: (message) => {
      console.log("User is sending message");
      let room = message.room;
      let msg = message.msg.message;
      //Make sure this user is in the room
      let rooms = Object.keys(socket.rooms);
      console.log(rooms);
      if (rooms.indexOf(room) === -1) {
        console.error('User is not logged into the room');
      } else if (socket.isAuth) {
        let newMessage = new Messages(null, msg, null, parseInt(room.split('-')[1]), socket.userId);
        socket.messageService.insertMessage(newMessage).then(async (result) => {
          const createdMessage = await socket.messageService.getMessage(result.insertId);
          const fileUpload = FileUploader.loadFromPost(message.msg);
          if (fileUpload.hasFile()) {
            createdMessage.image = fileUpload.saveFile(createdMessage.userId);
            await socket.messageService.updateMessage(createdMessage)
          }
          try {
            createdMessage.fromUser = await socket.userService.getUser(createdMessage.userId);
            const resp = new ApiResponse();
            resp.success = true;
            resp.data = {event: 'message', data: createdMessage};
            io.sockets.in(room).emit("message", resp);
          } catch(err) {
            console.error(err);
          }
        });
      } else {
        console.error("User is not authenticated");
      }
    },

    disconnectingRoute: () => {
      console.log("User is disconnecting");
      console.log(socket.rooms);
      if (socket.isAuth) {
        for (let room of Object.keys(socket.rooms)) {
          const resp = new ApiResponse();
          resp.success = true;
          resp.data = {event: 'userLeave', data: socket.userId};
          io.sockets.in(room).emit("event", resp);
        }
        console.log("Client has disconnected");
      } else {
        console.error("User is not authenticated");
      }
    }
  }
}