"use strict";

const GROUP_PERM = 'group';
const ADMIN_PERM = 'admin';

const express = require('express');
const router = express.Router();
const ApiResponse = require("../models/api_response");
const isAuthenticated = require("../includes/auth").isAuthenticated;
const hasGroupWrite = require("../includes/auth").hasGroupWrite;
const Groups = require("../models/groups");

function checkUserAuth(data, thisUser) {
  if (data.users.includes(thisUser.id)) {
    return true;
  }
  if (thisUser.permissions.includes(ADMIN_PERM)) {
    return true;
  }
  if (thisUser.permissions.includes(GROUP_PERM)) {
    if (data.admins.includes(thisUser.id)) {
      return true;
    }
  }
  return false;
}

module.exports = function () {
  router.get('/', isAuthenticated, function (req, res) {
    console.log("Request: /group get");
    res.setHeader('Content-Type', 'application/json');
    let response = new ApiResponse();
    let userId = req.params['id'];
    req.groupService.getGroups().then((data) => {
      response.data = data.filter((data) => {
          return checkUserAuth(data, req.thisUser);
        });
      response.success = true;

      res.send(response);
    }, (err) => {
      res.send(response);
    });
  });

  router.get('/:id', isAuthenticated, function (req, res) {
    console.log("Request: /group/:id get");
    res.setHeader('Content-Type', 'application/json');
    let response = new ApiResponse();
    let id = parseInt(req.params['id']);

    req.groupService.getGroup(id).then((groupModel) => {
      response.data = {group: groupModel};
      if (checkUserAuth(response.data.group, req.thisUser)) {
        // TODO: This needs to be async with await
        response.data.channels = req.channelService.getChannelsForGroup(response.data.group.id);
        req.userService.getUsersForGroup(response.data.group.id).then((userList) => {
          response.data.users = userList;
          response.success = true;
          res.send(response);
        }, (err) => {
          response.error = ["Database failure"];
          response.data = null;
          res.send(response);
        });

      } else {
        response.data.group = null;
        res.send(response);
      }
    }, (err) => {
      response.error = ["Database failure"];
      response.data = null;
      res.send(response);
    });

  });

  router.post('/', isAuthenticated, hasGroupWrite, function (req, res) {
    let response = new ApiResponse();
    console.log("Request: /group post");
    res.setHeader('Content-Type', 'application/json');

    let group = Groups.loadFromPost(req.body);
    response.success = req.groupService.insertGroup(group).then((data) => {
      response.success = true;

      res.send(response);
    }, (err) => {
      response.errors = ["Database failure"];
      res.send(response);
    });
  });

  router.delete('/:id', isAuthenticated, hasGroupWrite, function (req, res) {
    let response = new ApiResponse();
    console.log("Request: /group/:id delete");
    res.setHeader('Content-Type', 'application/json');

    let id = parseInt(req.params['id']);
    if (id > 0) {
      req.groupService.deleteGroup(id).then((data) => {
        response.success = true;

        res.send(response);
      }, (err) => {
        response.errors = ["Database error"];
        res.send(response);
      });
    } else {
      res.send(response);
    }
  });

  router.put('/:id', isAuthenticated, hasGroupWrite, function (req, res) {
    let response = new ApiResponse();
    console.log("Request: /group/:id put");
    res.setHeader('Content-Type', 'application/json');
    const id = parseInt(req.params['id']);

    let group = Groups.loadFromPost(req.body);
    group.id = id;
    
    req.groupService.updateGroup(group).then((data) => {
      response.success = true;

      res.send(response);
    }, (err) => {
      response.errors = ["Database failure"];
      res.send(response);
    });
  });

  return router;
};