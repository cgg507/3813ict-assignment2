"use strict";

const express = require('express');
const router = express.Router();
const ApiResponse = require("../models/api_response");
const isAuthenticated = require("../includes/auth").isAuthenticated;
const hasUserWrite = require("../includes/auth").hasUserWrite;
const Users = require("../models/users");
const FileUploader = require("../models/fileuploader");


module.exports = function () {
  router.get('/', isAuthenticated, function (req, res) {
    console.log("Request: /user get");
    res.setHeader('Content-Type', 'application/json');
    let response = new ApiResponse();
    req.userService.getUsers().then((data) => {
      response.success = true;
      response.data = data;
      res.send(response);
    }, (err) => {
      response.errors = ["Database failure"];
      res.send(response);
    });

  });

  router.get('/:id', isAuthenticated, function (req, res) {
    console.log("Request: /user:id get");
    res.setHeader('Content-Type', 'application/json');
    const id = parseInt(req.params['id']);
    let response = new ApiResponse();
    req.userService.getUser(id).then((data) => {
      response.success = true;
      response.data = data;

      res.send(response);
    }, (err) => {
      response.errors = ["Database failure"];
      res.send(response);
    });
  });

  router.post('/', isAuthenticated, hasUserWrite, function (req, res) {
    let response = new ApiResponse();
    console.log("Request: /user post");
    res.setHeader('Content-Type', 'application/json');
    const id = parseInt(req.params['id']);
    let user = Users.loadFromPost(req.body);
    user.id = id;
    req.userService.insertUser(user).then(async (data) => {
      response.success = true;
      let fileUploader = FileUploader.loadFromPost(req.body);
      if (fileUploader.hasFile()) {
        console.log("User created and uploading file: " + data.insertId);
        const createdUser = await req.userService.getUser(data.insertId);
        createdUser.image = fileUploader.saveFile(createdUser.id);
        response.data = createdUser;
        if (user.image != null) {
          await req.userService.updateUser(createdUser);
        }
      }
      res.send(response);
    }, (err) => {
      response.errors = ["Database failure"];
      res.send(response);
    });
  });

  router.delete('/:id', isAuthenticated, hasUserWrite, function (req, res) {
    let response = new ApiResponse();
    console.log("Request: /user delete");
    res.setHeader('Content-Type', 'application/json');
    const id = parseInt(req.params['id']);
    req.userService.deleteUser(id).then((data) => {
      response.success = true;
      res.send(response);
    }, (err) => {
      response.errors = ["Database failure"];
      res.send(response);
    });
  });

  router.put('/:id', isAuthenticated, hasUserWrite, function (req, res) {
    let response = new ApiResponse();
    res.setHeader('Content-Type', 'application/json');
    console.log("Request: /user put");
    const id = parseInt(req.params['id']);
    
    let user = Users.loadFromPost(req.body);
    if (req.thisUser.permissions.indexOf('admin') !== -1) {
      console.log("User is admin");
      if (req.body.permissions != null) {
        console.log("Changing user permissions");
        user.permissions = req.body.permissions;
      }
    }
    let fileUploader = FileUploader.loadFromPost(req.body);
    if (fileUploader.hasFile()) {
      user.image = fileUploader.saveFile(id);
    }
    user.id = id;
    req.userService.updateUser(user).then(async (data) => {
      try {
        response.data = await req.userService.getUser(id);
        response.success = true;
      } catch(err) {
        response.success = false;
      }
      res.send(response);
    }, (err) => {
      response.errors = ["Database failure"];
      res.send(response);
    });
  });

  return router;
};