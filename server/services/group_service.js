"use strict";

const User = require("../models/users");



module.exports = function groupService(db) {
  const collection = db.collection('groups');
  return {
    getGroups: () => {
      return new Promise((resolve, reject) => {
        collection.find().toArray((err, result) => {
          if (err === null) {
            resolve(result);
          } else {
            reject(err);
          }
        });
      });
    },
    getGroup: (id) => {
      return new Promise((resolve, reject) => {
        collection.find({id: id}).toArray((err, result) => {
          if (err === null) {
            resolve(result[0]);
          } else {
            reject(err);
          }
        });
      });
    },

    insertGroup: (group) => {
      return new Promise((resolve, reject) => {
        const autoInc = collection.find().sort({id: -1}).limit(1);
        autoInc.next((err, nextGroup) => {
          if (nextGroup !== null) {
            group.id = nextGroup.id + 1;
          } else {
            group.id = 1;
          }

          collection.insertOne(group, (err, result) => {
            if (err === null) {
              result.insertId = group.id;
              resolve(result);
            } else {
              reject(err);
            }
          });
        });
      });
    },

    updateGroup: (group) => {
      return new Promise((resolve, reject) => {
        collection.find({id: group.id}).toArray((err, oldData) => {

          oldData[0].name = group.name;
          oldData[0].users = group.users == null ? [] : group.users;
          oldData[0].admins = group.admins == null ? [] : group.admins;

          collection.updateOne({id: oldData.id}
            , {$set: oldData[0]}, (err, result) => {
              if (err === null) {
                resolve(result);
              } else {
                reject(err);
              }
            });
        });
      });
    },

    deleteGroup: (id) => {
      return new Promise((resolve, reject) => {
        collection.remove({id: id}, (err, result) => {
          if (err === null) {
            resolve(result);
          } else {
            reject(err);
          }
        });
      });
    },
  };
};
