"use strict";

module.exports = function userService(db) {
  const collection = db.collection('users');
  const sessionCollection = db.collection('sessions');
  const groupCollection = db.collection('groups');
  return {
    getUsers: (publicData = false) => {
      return new Promise((resolve, reject) => {
        collection.find(null, publicData ? {firstName: 1, lastName: 1, id: 1} : null).toArray((err, data) => {
          if (err === null) {
            resolve(data);
          } else {
            reject(err);
          }
        });
      });
    },
    getUser: (id, publicData = false) => {
      return new Promise((resolve, reject) => {
        collection.find({id: id}, publicData ? {firstName: 1, lastName: 1, id: 1} : null).toArray((err, data) => {
          if (data.length < 1) {
            reject(err);
            return;
          }
          if (err !== null) {
            reject(err);
          } else {
            resolve(data[0]);
          }
        });
      });
    },

    getUserByToken: (token) => {
      return new Promise((resolve, reject) => {
        sessionCollection.find({session: "" + token}).toArray((err, data) => {
          collection.find({id: data[0].userId}).toArray((err, data) => {
            if (data.length < 1) {
              reject(err);
              return;
            }
            if (err !== null) {
              reject(err);
            } else {
              resolve(data[0]);
            }
          });
        });
      });
    },

    findByEmail: (email) => {
      return new Promise((resolve, reject) => {
        collection.find({email: email}).toArray((err, data) => {
          if (data.length < 1) {
            reject(err);
            return;
          }
          if (err !== null) {
            reject(err);
          } else {
            resolve(data[0]);
          }
        });
      });
    },

    getUsersForGroup: (groupId) => {
      return new Promise((resolve, reject) => {
        groupCollection.find({id: groupId}).toArray((err, group) => {
          if (group.length < 1 || err !== null) {
            console.error("Failed to find group");
            reject(err);
            return;
          }
          collection.find({id: {$in: group[0].users}}).toArray((err, result) => {
            if (err !== null) {
              reject(err);
            } else {
              resolve(result);
            }
          });
        });
      });
    },


    /**
     *
     * @param user
     * @param callback
     */
    insertUser: (user) => {
      return new Promise((resolve, reject) => {
        const autoInc = collection.find().sort({id: -1}).limit(1);
        autoInc.next((err, nextUser) => {
          if (nextUser !== null) {
            user.id = nextUser.id + 1;
          } else {
            user.id = 1;
          }

          user.permissions = user.permissions == null ? [] : user.permissions;
          collection.insertOne(user, (err, result) => {
            if (err !== null) {
              reject(err);
            } else {
              result.insertId = user.id;
              resolve(result);
            }
          });
        });
      });
    },

    updateUser: (user) => {
      return new Promise((resolve, reject) => {
        collection.find({id: user.id}).toArray((err, oldUser) => {
          if (err) {
            reject(err);
            return;
          }
          const userId = user.id;
          oldUser[0].email = user.email;
          oldUser[0].password = user.password;
          oldUser[0].firstName = user.firstName;
          oldUser[0].lastName = user.lastName;
          if (user.permissions != null)
            oldUser[0].permissions = user.permissions;
          if (user.password != null) {
            oldUser[0].password = user.password;
          }
          if (user.image != null) {
            console.log('Updating the image');
            oldUser[0].image = user.image;
          }
          collection.updateOne({id: oldUser[0].id}
            , {$set: oldUser[0]}, (err, result) => {
              if (err !== null) {
                reject(err);
              } else {
                resolve(result);
              }
            });
        });
      });
    },
    
    deleteUser: (id) => {
      return new Promise((resolve, reject) => {
        collection.remove({id: id}, (err, data) => {
          if (err === null) {
            resolve(data);
          } else {
            reject(err);
          }
        });
      });
    },
  };
};
