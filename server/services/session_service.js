"use strict";

const Sessions = require('../models/sessions');

module.exports = function sessionService(db) {
  const collection = db.collection('sessions');
  return {
    checkSession: (session) => {
      return new Promise((resolve, reject) => {
        collection.find({session: "" + session}).toArray((err, data) => {
          if (err === null) {
            resolve(data[0]);
          } else {
            reject(err);
          }
        });
      });
    },

    insertSession: (userId, session) => {
      return new Promise((resolve, reject) => {
        const sessionModel = new Sessions("" + session, parseInt(userId));
        collection.insertOne(sessionModel, (err, data) => {
          if (err === null) {
            resolve(data);
          } else {
            reject(err);
          }
        });
      });
    },
    
    removeSession(userId, session) {
      return new Promise((resolve, reject) => {
        collection.remove({session: "" + session, userId: parseInt(userId)}, (err, data) => {
          if (err === null) {
            resolve(data);
          } else {
            reject(err);
          }
        });
      });
    }

  };
};
