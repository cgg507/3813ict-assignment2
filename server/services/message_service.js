"use strict";

const User = require("../models/users");

module.exports = function channelService(db) {
  const collection = db.collection('messages');
  return {
    getMessages: (channelId = null) => {
      return new Promise((resolve, reject) => {
        let query = null;
        if (channelId != null) {
          query = {channelId: parseInt(channelId)};
        }
        collection.find(query).toArray((err, data) => {
          if (err === null) {
            resolve(data);
          } else {
            reject(err);
          }
        });
      });
    },
    getMessage: (id) => {
      return new Promise((resolve, reject) => {
        collection.find({id: id}).toArray((err, data) => {
          if (err !== null) {
            reject(err);
          } else {
            resolve(data[0]);
          }
        });
      });
    },
    
    insertMessage: (message) => {
      return new Promise((resolve, reject) => {
        const autoInc = collection.find().sort({id: -1}).limit(1);
        autoInc.next((err, nextMessage) => {
          if (nextMessage !== null) {
            message.id = nextMessage.id + 1;
          } else {
            message.id = 1;
          }
          collection.insertOne(message, (err, result) => {
            if (err !== null) {
              reject(err);
            } else {
              result.insertId = message.id;
              resolve(result);
            }
          });
        });
      });
    },

    updateMessage: (message) => {
      return new Promise((resolve, reject) => {
        collection.find({id: message.id}).toArray((err, oldData) => {
          if (message.message != null)
            oldData[0].message = message.message;
          if (message.image != null)
            oldData[0].image = message.image;

          collection.updateOne({id: oldData[0].id}
            , {$set: oldData[0]}, (err, result) => {
              if (err !== null) {
                reject(err);
              } else {
                resolve(result);
              }
            });
        });
      });
    },

    deleteMessage: (id) => {
      return new Promise((resolve, reject) => {
        collection.remove({id: id}, (err, result) => {
          if (err !== null) {
            reject(err);
          } else {
            resolve(result);
          }
        });
      });
    },
  };
};
