"use strict";

const User = require("../models/users");

module.exports = function channelService(db) {
  const collection = db.collection('channels');
  return {
    getChannels: (groupId = null) => {
      return new Promise((resolve, reject) => {
        let query = null;
        if (groupId != null) {
          query = {groupId: parseInt(groupId)};
          console.log(query);
        }
        collection.find(query).toArray((err, result) => {
          if (err === null) {
            resolve(result);
          } else {
            reject(err);
          }
        });
      });
    },
    getChannelsForGroup: (groupId) => {
      return new Promise((resolve, reject) => {
        collection.find({groupId: groupId}).toArray((err, result) => {
          if (err === null) {
            resolve(result);
          } else {
            reject(err);
          }
        });
      });
    },
    getChannel: (id) => {
      return new Promise((resolve, reject) => {
        collection.find({id: id}).toArray((err, result) => {
          if (err === null) {
            resolve(result[0]);
          } else {
            reject(err);
          }
        });
      });
    },
    
    insertChannel: (channel) => {
      return new Promise((resolve, reject) => {
        const autoInc = collection.find().sort({id: -1}).limit(1);
        autoInc.next((err, nextChannel) => {
          if (nextChannel !== null) {
            channel.id = nextChannel.id + 1;
          } else {
            channel.id = 1;
          }
          collection.insertOne(channel,(err, result) => {
            if (err === null) {
              result.insertId = channel.id;
              resolve(result);
            } else {
              reject(err);
            }
          });
        });
      });
    },

    updateChannel: (channel) => {
      return new Promise((resolve, reject) => {
        collection.find({id: channel.id}).toArray((err, oldData) => {
          if (channel.name != null)
            oldData[0].name = channel.name;
          if (channel.groupId != null)
            oldData[0].groupId = parseInt(channel.groupId);
          if (channel.users != null)
            oldData[0].users = channel.users;
          collection.updateOne({id: oldData.id}
            , {$set: oldData[0]}, function (err, result) {
              if (err === null) {
                resolve(result);
              } else {
                reject(err);
              }
            });
        });
      });
    },

    deleteChannel: (id) => {
      return new Promise((resolve, reject) => {
        collection.remove({id: id}, (err, result) => {
          if (err === null) {
            resolve(result);
          } else {
            reject(err);
          }
        });
      });
    },
  };
};
