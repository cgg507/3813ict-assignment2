"use strict";

class Users {
    constructor(id, email, password, firstName, lastName, permissions, image) {
        this.id = parseInt(id);
        this.email = email;
        this.image = image;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.permissions = permissions;
    }
    
    static loadFromPost(body) {
        return new Users(parseInt(body.id), body.email, body.password, body.firstName, body.lastName);
    }

}

module.exports = Users;