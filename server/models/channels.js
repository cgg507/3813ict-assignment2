"use strict";

class Channels {
    constructor(id, name, groupId, users) {
        this.id = id;
        this.name = name;
        this.groupId = groupId;
        this.users = users;
        this.group = null;
        this.messages = [];
    }

  static loadFromPost(body) {
    return new Channels(parseInt(body.id), body.name, parseInt(body.groupId), body.users);
  }

}

module.exports = Channels;