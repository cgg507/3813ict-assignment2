"use strict";

class Messages {
    constructor(id, message, image, channelId, userId) {
        this.id = id;
        this.message = message;
        this.image = image;
        this.userId = parseInt(userId);
        this.channelId = parseInt(channelId);
        this.fromUser = null;
        this.createDate = new Date().toString();
    }

  static loadFromPost(body) {
    return new Messages(parseInt(body.id), body.message, null, parseInt(body.channelId), null);
  }

}

module.exports = Messages;