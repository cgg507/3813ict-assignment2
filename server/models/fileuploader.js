const fs = require('fs');

class FileUploader {
  constructor(fileUpload, fileName) {
    this.fileUpload = fileUpload;
    this.fileName = fileName;
  }

  static loadFromPost(body) {
    return new FileUploader(body.fileUpload, body.fileName);
  }

  hasFile() {
    return this.fileUpload != null && this.fileUpload !== "";
  }

  saveFile(userId) {
    let bitmap = new Buffer(this.fileUpload.replace(/^data:image\/[A-Za-z-0-9_]*;base64/, ''), 'base64').toString('binary');
    let userDir = __dirname + '/../../uploads/' + userId;
    let newFile = "/" + (new Date().getTime()) + "-" + this.fileName;
    try {
      fs.mkdirSync(userDir)
    } catch (err) {
      if (err.code !== 'EEXIST') {
        console.error(err);
        return null;
      }
    }

    try {
      fs.writeFileSync(userDir + newFile, bitmap, 'binary');
    } catch (err) {
      console.error(err);
      return null;
    }
    return newFile;
  }
}

module.exports = FileUploader;