"use strict";

class Sessions {
  constructor(session, userId) {
    this.session = session;
    this.userId = parseInt(userId);
  }
}

module.exports = Sessions;